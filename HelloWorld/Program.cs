﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static double number;

        static void Main(string[] args)
        {
            // Call Print Message to show new message
            PrintMessage("This is feature 1");

            number = AddTwoNumber(12.3, 12.34);
            PrintNumber();
        }

        public static void PrintMessage(string message)
        {
            System.Console.WriteLine(message);
        }

        public static void PrintNumber()
        {
            System.Console.WriteLine(number);
        }

        public static Double AddTwoNumber(double no1, double no2)
        {
            return no1 + no2;
        }
    }
}
